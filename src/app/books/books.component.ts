import {Component, OnInit} from '@angular/core';
import {Book} from '../model/book';
import {Apollo, gql} from 'apollo-angular';

export const GET_BOOKS = gql`
  query MyQuery {
  books {
    author
    id
    title
  }
}
`

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  displayedColumns: string[] = [
    'id',
    'title',
    'author'
  ]

  // allBooks: Book[] = [
  //   {id: 1, title: '1984', author: 'George Orwell'},
  //   {id: 2, title: 'Brave New World', author: 'Aldous Huxley'},
  //   {id: 3, title: 'Norwegian Wood', author: 'Haruki Murakami'}
  // ]

  allBooks: Book[] = [];

  constructor(private apollo: Apollo) {
  }

  ngOnInit(): void {
    this.apollo.watchQuery({
      query: GET_BOOKS
    }).valueChanges
      .subscribe(({data}) => {
        console.log(data);
        // @ts-ignore
        this.allBooks = data.books;
      });
  }

  sendQuery() {

  }
}
