import {Component, OnInit} from '@angular/core';
import {gql, Apollo} from 'apollo-angular';
import {GET_BOOKS} from "../books/books.component";

const POST_BOOK = gql`
mutation PostBook($title: String, $author: String) {
  insert_books(objects: {title: $title, author: $author}) {
    returning {
      id
      title
      author
    }
  }
}
`

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  title: string | undefined;
  author: string | undefined;

  constructor(private apollo: Apollo) {
  }

  ngOnInit(): void {
  }

  handleSubmit(): void {
    alert("Submitted! TItle: " + this.title + ", author: " + this.author);

    this.apollo.mutate({
      mutation: POST_BOOK,
      variables: {
        title: this.title,
        author: this.author
      },
      update: (cache, {data}) => {
        const existingBooks : any = cache.readQuery({
          query: GET_BOOKS
        });

        // @ts-ignore
        const newBook = data.insert_books.returning[0];
        cache.writeQuery({
          query: GET_BOOKS,
          data: {books: [...existingBooks.books, newBook]}
        });
      }
    }).subscribe(({data}) => {

    }, (error) => {
      alert("There was an error sending the query: " + error);
    })
  }

}
